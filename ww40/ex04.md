Frame: Viser dataen der bliver sendt. 

Ethernet II src: MAC-adresse på sender og modtager.

Internet protocol: protocol version og ip på sender og modtager.

![Ping 8.8.8.8](https://gitlab.com/MartinHvid/networking-basics/-/raw/master/ww40/ww40ex04Ping8.8.8.8.JPG)
![Ping 1.1.1.1](https://gitlab.com/MartinHvid/networking-basics/-/raw/master/ww40/ww40ex04Ping1.1.1.1.JPG)