Kommandoen "frigiver" den tildelte IP adresse fra serveren. så serveren ved at man ikke ønsker at bruge den IP mere?

![Kommando](https://gitlab.com/MartinHvid/networking-basics/-/raw/master/ww41/ww41ex03.JPG)

og så skal man selv tildele sig en ny ip?

![Static IP](https://gitlab.com/MartinHvid/networking-basics/-/raw/master/ww41/ww41ex03Static.JPG)

![Ping Gateway & 8.8.8.8](https://gitlab.com/MartinHvid/networking-basics/-/raw/master/ww41/ww41ex03PingGW_Inet.JPG)
